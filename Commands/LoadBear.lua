function getInfo()
	return {
		onNoUnits = SUCCESS, -- instant success
		tooltip = "Load units.",
		parameterDefs = {
			{ 
				name = "theBear",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
			{ 
				name = "unitList",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			}
		}
	}
end

function Run(self, units, parameter)
	local theBear = parameter.theBear
	local unitList = parameter.unitList

	--give orders
	for i=1,#unitList do
		Spring.GiveOrderToUnit(theBear,CMD.LOAD_UNITS,{unitList[i]},{})
	end
	
	--check if done
	allIn = true
	for i = 1, #unitList do
	    if not Spring.GetUnitTransporter(unitList[i]) then
	        allIn = false
		end
	end
	
	if theBear==nil or not Spring.ValidUnitID(theBear) then return SUCCESS end --TODO: success or failure?
		
	--if self:UnitIdle(theBear) then
	if allIn==true then
		return SUCCESS
	else
		return RUNNING
	end
	
end


function Reset(self)
end
