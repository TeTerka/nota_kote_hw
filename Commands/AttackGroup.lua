function getInfo()
	return {
		onNoUnits = FAILURE, -- instant success
		tooltip = "Attack target until it is dead.",
		parameterDefs = {
			{ 
				name = "myUnits",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
			{ 
				name = "enemyUnitID",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			}
		}
	}
end

function Run(self, units, parameter)
	local us = parameter.myUnits
	local enemy = parameter.enemyUnitID
	
	--if not Spring.ValidUnitID(me) then return FAILURE end

	if us==nil or #us==0 then return FAILURE end
	
	allDead = true
	for i=1,#us do
		me = us[i]
		if me~=nil and Spring.ValidUnitID(me) then allDead = false end
		
		if Spring.ValidUnitID(enemy) then
			local x,y,z = Spring.GetUnitPosition(enemy)
			Spring.GiveOrderToUnit(me,CMD.FIGHT,{x,y,z},{})
		else
			Spring.GiveOrderToUnit(me,CMD.STOP,{},{})
			return SUCCESS
		end
	end
	
	if allDead then return FAILURE end
	
	return RUNNING
	
end


function Reset(self)
end
