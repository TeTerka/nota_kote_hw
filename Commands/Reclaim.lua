function getInfo()
	return {
		onNoUnits = FAILURE, -- instant success
		tooltip = "Attack target until it is dead.",
		parameterDefs = {
			{ 
				name = "unitID",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
			{ 
				name = "pos",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			}
		}
	}
end

function Run(self, units, parameter)
	local me = parameter.unitID
	local pos = parameter.pos
	
	Spring.GiveOrderToUnit(me,CMD.RECLAIM,{pos.x,pos.y,pos.z,200},{})
	
	if self:UnitIdle(me) then
		return SUCCESS
	else
		return RUNNING
	end
	
end


function Reset(self)
end
