function getInfo()
	return {
		onNoUnits = FAILURE, -- instant success
		tooltip = "Attack target until it is dead.",
		parameterDefs = {
			{ 
				name = "unitID",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			}
		}
	}
end

function Run(self, units, parameter)

	local me = parameter.unitID
	Spring.GiveOrderToUnit(me,CMD.STOP,{},{})
	return SUCCESS
	
end


function Reset(self)
end
