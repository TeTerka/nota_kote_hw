function getInfo()
	return {
		onNoUnits = FAILURE, -- instant success
		tooltip = "Unload units.",
		parameterDefs = {
			{ 
				name = "theBear",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
			{ 
				name = "unloadPos",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
			{ 
				name = "movePosAfterUnload",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			}
		}
	}
end
local function ClearState(self)
	self.orderGiven = false
end

function Run(self, units, parameter)
	local theBear = parameter.theBear
	local unloadPos = parameter.unloadPos
	local movePos = parameter.movePosAfterUnload
	
	if not self.orderGiven then
		Spring.GiveOrderToUnit(theBear,CMD.TIMEWAIT,{1},{"shift"})
		Spring.GiveOrderToUnit(theBear,CMD.UNLOAD_UNITS,{unloadPos.x,unloadPos.y,unloadPos.z,10},{"shift"})
		Spring.GiveOrderToUnit(theBear,CMD.MOVE,movePos:AsSpringVector(),{"shift"})
		self.orderGiven = true
	end
	
	if theBear==nil or not Spring.ValidUnitID(theBear) then return SUCCESS end
	
	x,y,z = Spring.GetUnitPosition(theBear)
		if Vec3(x,0,z):Distance(movePos)<300 then
			return SUCCESS
		else
			return RUNNING
		end
	
	if self:UnitIdle(theBear) then
		return SUCCESS
	else
		return RUNNING
	end
end


function Reset(self)
	ClearState(self)
end
