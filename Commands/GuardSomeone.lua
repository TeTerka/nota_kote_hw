function getInfo()
	return {
		onNoUnits = FAILURE, -- instant success
		tooltip = "Attack target until it is dead.",
		parameterDefs = {
			{ 
				name = "unitID",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
			{ 
				name = "enemyUnitID",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			}
		}
	}
end

function Run(self, units, parameter)
	local me = parameter.unitID
	local enemy = parameter.enemyUnitID
	
	--if not Spring.ValidUnitID(me) then return FAILURE end

	if Spring.ValidUnitID(enemy) then
		Spring.GiveOrderToUnit(me,CMD.ATTACK,{enemy},{})
		return RUNNING
	else
		Spring.GiveOrderToUnit(me,CMD.STOP,{},{})
		return SUCCESS
	end
	
end


function Reset(self)
end
