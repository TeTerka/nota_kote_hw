function getInfo()
	return {
		onNoUnits = FAILURE, -- instant success
		tooltip = "Walk along the path.",
		parameterDefs = {
			{ 
				name = "unitID",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
			{ 
				name = "path",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			}			,
			{ 
				name = "backwards",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "false",
			},
			{
				name = "radius",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "300",
			}
		}
	}
end


local function ClearState(self)
	self.orderGiven = nil
end

function Run(self, units, parameter)

	local myID = parameter.unitID
	local thePath = parameter.path
	local backwards = parameter.backwards
	local radius = parameter.radius
	
	if thePath==nil or #thePath<1 or myID==nil or not Spring.ValidUnitID(myID) then return FAILURE end
	
	--TODO: if standing too long on one spot -> FAILURE
	
	local pointX, pointY, pointZ = Spring.GetUnitPosition(myID)
	local myPos = Vec3(pointX, pointY, pointZ)
			
	if backwards==true then 
		if self.orderGiven==nil then
			for i=#thePath,1,-1 do
				if Sensors.nota_kote_hw.test_detectCorpses(thePath[i],radius) then
					Spring.GiveOrderToUnit(myID, CMD.RECLAIM, {thePath[i].x,thePath[i].y,thePath[i].z, radius}, {"shift"})
				end
				Spring.GiveOrderToUnit(myID, CMD.MOVE, thePath[i]:AsSpringVector(), {"shift"})
			end
			self.orderGiven = 1
		end
		
		if Vec3(myPos.x,0,myPos.z):Distance(thePath[1])<300 then
			return SUCCESS
		else
			return RUNNING
		end
	else			
		if self.orderGiven==nil then
			for i=1,#thePath do
				if Sensors.nota_kote_hw.test_detectCorpses(thePath[i],radius) then
					Spring.GiveOrderToUnit(myID, CMD.RECLAIM,{thePath[i].x,thePath[i].y,thePath[i].z, radius}, {"shift"})
				end
				Spring.GiveOrderToUnit(myID, CMD.MOVE, {thePath[i].x,thePath[i].y,thePath[i].z}, {"shift"})
			end
			self.orderGiven = 1
		end
		
		n=10000
						--debug...
				if (Script.LuaUI('exampleDebug_update')) then
					Script.LuaUI.exampleDebug_update(
					n, -- key
					{	-- data
						startPos = Vec3(thePath[#thePath].x-50,0,thePath[#thePath].z), 
						endPos = Vec3(thePath[#thePath].x+50,0,thePath[#thePath].z)
					}
					)
				end
				n = n+1
								if (Script.LuaUI('exampleDebug_update')) then
					Script.LuaUI.exampleDebug_update(
					n, -- key
					{	-- data
						startPos = Vec3(thePath[#thePath].x,0,thePath[#thePath].z+50), 
						endPos = Vec3(thePath[#thePath].x,0,thePath[#thePath].z-50)
					}
					)
				end
				n = n+1
		
		
		--TODO: if standing too long on one spot -> FAILURE
		if self:UnitIdle(myID) then return FAILURE end
		
		if Vec3(myPos.x,0,myPos.z):Distance(thePath[#thePath])<=radius then
			return SUCCESS
		else
			return RUNNING
		end
	end

end

function Reset(self)
	ClearState(self)
end
