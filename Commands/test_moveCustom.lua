function getInfo()
	return {
		onNoUnits = FAILURE, 
		tooltip = "Move.",
		parameterDefs = {
			{ 
				name = "allUnits",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
			{ 
				name = "targetPos",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
			{ 
				name = "fight",
				variableType = "expression",
				componentType = "checkBox",
				defaultValue = "true",
			}
		}
	}
end

function Run(self, units, parameter)
	local us = parameter.unitID
	local vecPos = parameter.targetPos
	local fight = parameter.fight
	--vecPos = Vec3(pos[1],pos[2],pos[3])
	
	c = CMD.MOVE
	if fight then
		c = CMD.FIGHT
	end
	
	if us==nil or #us==0 then return FAILURE end
	
	allDead = true
	for i=1,#us do
		me = us[i]
		if me~=nil and Spring.ValidUnitID(me) then 
			allDead = false
			Spring.GiveOrderToUnit(me,c,{vecPos.x,vecPos.y,vecPos.z},{})
		end
	end
	
	--if self:UnitIdle(myID) then return FAILURE end
	
	if allDead then return FAILURE end

	x,y,z = Spring.GetUnitPosition(me)
	if Vec3(x,y,z):Distance(vecPos)<250 then
		return SUCCESS
	else
		return RUNNING
	end
	
end


function Reset(self)
	self.orderGiven = false
end
