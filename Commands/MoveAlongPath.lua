function getInfo()
	return {
		onNoUnits = FAILURE, -- instant success
		tooltip = "Walk along the path.",
		parameterDefs = {
			{ 
				name = "unitID",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
			{ 
				name = "path",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			}			,
			{ 
				name = "backwards",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "false",
			}
		}
	}
end


local function ClearState(self)
	self.orderGiven = nil
end

function Run(self, units, parameter)

	local myID = parameter.unitID
	local thePath = parameter.path
	local backwards = parameter.backwards
	
	if thePath==nil or #thePath<1 or not Spring.ValidUnitID(myID) then return FAILURE end
	

	
	local pointX, pointY, pointZ = Spring.GetUnitPosition(myID)
	local myPos = Vec3(pointX, pointY, pointZ)
			
	if backwards==true then 
		if self.orderGiven==nil then
			for i=#thePath,1,-1 do
				Spring.GiveOrderToUnit(myID, CMD.MOVE, thePath[i]:AsSpringVector(), {"shift"})
			end
			self.orderGiven = 1
		end
		
		if Vec3(myPos.x,0,myPos.z):Distance(thePath[1])<300 then
			return SUCCESS
		else
			return RUNNING
		end
	else			
		if self.orderGiven==nil then
			Spring.GiveOrderToUnit(myID, CMD.STOP, {}, {})
			for i=1,#thePath do
				Spring.GiveOrderToUnit(myID, CMD.MOVE, thePath[i]:AsSpringVector(), {"shift"})
			end
			self.orderGiven = 1
		end
		
		--TODO: if standing too long on one spot -> FAILURE
		if self:UnitIdle(myID) then return FAILURE end

		if Vec3(myPos.x,0,myPos.z):Distance(thePath[#thePath])<600 then
			Spring.GiveOrderToUnit(myID, CMD.STOP, {}, {})
			return SUCCESS
		else
			return RUNNING
		end
	end


end

function Reset(self)
	ClearState(self)
end
