local sensorInfo = {
	name = "FormationCreator",
	desc = "",
	author = "",
	date = "2017-05-16",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end


-- @description creates a group for formation.moveCustomGroup from a list of units
return function(listOfUnits)
	group = {}
	for i=1,#listOfUnits do
		group[listOfUnits[i]] = i
	end
	return group
end