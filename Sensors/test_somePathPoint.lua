local sensorInfo = {
	name = "SomePathPoint",
	desc = "",
	author = "",
	date = "2017-05-16",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1 

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

-- @description a path point in the "fraction" of the path
return function(path,fraction)	
	
	if fraction>1 or fraction <=0 then return path[1] end

	count = #path
	safe = math.ceil(count * fraction)
	return path[safe]
end