local sensorInfo = {
	name = "AllyDetector",
	desc = "",
	author = "",
	date = "2017-05-16",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1 

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

-- @description 
return function(group)	
	
	if group==nil then return true end
	
	alldead = true
	for i=1,#group do
		if group[i]~=nil and Spring.ValidUnitID(group[i]) then
			alldead = false
			break
		end
	end
	return alldead
end