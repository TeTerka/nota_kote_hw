local sensorInfo = {
	name = "EvaluateGrid",
	desc = "",
	author = "tk",
	date = "2017-05-16",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = math.huge -- jen jednou

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

--enemies is a list of units
--obsatcles is a list of vectors (positions)
return function(enemies,obstacles)
				
		local grid = {}
		local dist = 600
		
		local n = 0
		local minH, maxH = Spring.GetGroundExtremes()
		for i=1, Game.mapSizeX, 100 do
			local ii = math.floor(i/100)
			grid[ii] = {}
			for j=1, Game.mapSizeZ, 100 do
			
				local jj = math.floor(j/100)
				grid[ii][jj] = 1
				for k=1,#enemies do
					local eX,eY,eZ = Spring.GetUnitPosition(enemies[k])
					if math.abs(eX-i)<dist and math.abs(eZ-j)<dist 
							and minH+180<=Spring.GetGroundHeight(i,j) then
						grid[ii][jj] = grid[ii][jj] + 10
						break 
					end
				end
				for l=1,#obstacles do
					if math.abs(obstacles[l].x-i)<dist and math.abs(obstacles[l].z-j)<dist 
							and minH+180<=Spring.GetGroundHeight(i,j) then
						grid[ii][jj] = 11 
						break 
					end
				end
				
				if grid[ii][jj]>10 then
				--debug...
				if (Script.LuaUI('exampleDebug_update')) then
					Script.LuaUI.exampleDebug_update(
					n, -- key
					{	-- data
						startPos = Vec3(i-10,maxH,j), 
						endPos = Vec3(i+10,maxH,j)
					}
					)
				end
				n = n+1
				end
			end
		end	
		
		
		--second loop - refining the grid
		for i=1, Game.mapSizeX, 100 do
			local ii = math.floor(i/100)
			for j=1, Game.mapSizeZ, 100 do
			
				local jj = math.floor(j/100)--if all neighbours are not unsafe
				if grid[ii][jj] == 1 and grid[ii+1]~=nil and grid[ii+1][jj]~=11 and grid[ii-1]~=nil and grid[ii-1][jj]~=11 and grid[ii][jj+1]~=11 and grid[ii][jj-1]~=11 then
					grid[ii][jj] = 1
				else
					grid[ii][jj] = 111
					--debug
					if (Script.LuaUI('exampleDebug_update')) then
						Script.LuaUI.exampleDebug_update(
						n, -- key
						{	-- data
							startPos = Vec3(i-10,maxH,j), 
							endPos = Vec3(i+10,maxH,j)
						}
						)
					end
					n = n+1
				--end of debug
				end
			end
		end
		
		

		return grid
end