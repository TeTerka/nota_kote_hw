local sensorInfo = {
	name = "",
	desc = "",
	author = "",
	date = "2017-05-16",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1 

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

-- @description 
return function(pathTargets)	
		
	for i=1,#pathTargets do
		if pathTargets[i]~=nil and Spring.ValidUnitID(pathTargets[i]) then
			return pathTargets[i]
		end
	end
	
	return nil
end