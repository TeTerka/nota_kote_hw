local sensorInfo = {
	name = "UnitFilter",
	desc = "",
	author = "tk",
	date = "2018-04-25",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

-- @description return filtered list of units
return function(path)	

		pathpoints = {}
		for i=1,#path do
			pathpoints[i] = path[i].position
		end
		
		return pathpoints
		
end