local sensorInfo = {
	name = "CorpseDetector",
	desc = "",
	author = "",
	date = "2017-05-16",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1 

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

-- @description is there some corpse nearby?
return function(pos, range)	
		
	listFeaturesArea = Spring.GetFeaturesInSphere(pos.x,pos.y,pos.z,range)
	if listFeaturesArea==nil then return false end
	
	hasMetal = false
	for k = 1, #listFeaturesArea do 	
		remainingMetal,_,_,_,_ = Spring.GetFeatureResources(listFeaturesArea[k])
		if remainingMetal>0 then
			hasMetal = true
			break
		end
	end
	
	return hasMetal
end