local sensorInfo = {
	name = "AssignFarcks",
	desc = "look for idle farcks an assign them empty paths",
	author = "tk",
	date = "2017-05-16",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = 0

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end


return function(allFarcks)

		--check if we have any farcks
		if allFarcks==nil or #allFarcks==0 then return 0 end
		
		--check who is assign to which line
		currentTop = bb.topFarck
		currentBottom = bb.bottomFarck
		
		--if there is no farck for the top line try to assign a new one
		if currentTop==nil or not Spring.ValidUnitID(currentTop) then
			for i=1, #allFarcks do
				if allFarcks[i]~=currentBottom then
					bb.topFarck = allFarcks[i]
					break
				end
			end
		end
		
		--same for bottom line
		if currentBottom==nil or not Spring.ValidUnitID(currentBottom) then
			for i=1, #allFarcks do
				if allFarcks[i]~=bb.topFarck then
					bb.bottomFarck = allFarcks[i]
					break
				end
			end
		end
		
		
	return 0
end