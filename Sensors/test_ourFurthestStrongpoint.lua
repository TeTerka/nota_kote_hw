local sensorInfo = {
	name = "",
	desc = "",
	author = "",
	date = "2017-05-16",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1 

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

-- @description 
return function(ourAllience, points)	
		
	s = points[1].position
	for i=1,#points do
		if points[i].isStrongpoint==true and points[i].ownerAllyID==ourAllience then
			s = points[i].position
		end
	end
	
	return Vec3(s.x,s.y,s.z)
end