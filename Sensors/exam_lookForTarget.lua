local sensorInfo = {
	name = "",
	desc = "",
	author = "",
	date = "2017-05-16",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1 

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

-- @description 
return function(me, radius, pathTargets)	
		
	x,y,z = Spring.GetUnitPosition(me)
	myPos = Vec3(x,y,z)
	nearest = nil
	dist = radius
	
	for i=1,#pathTargets do
		if pathTargets[i]~=nil and Spring.ValidUnitID(pathTargets[i]) then
			a,b,c = Spring.GetUnitPosition(pathTargets[i])
			enemyPos = Vec3(a,b,c)
			if myPos:Distance(enemyPos)<dist then
				dist = myPos:Distance(enemyPos)
				nearest = pathTargets[i]
			end
		end
	end
	
	return nearest
	
	--toto je mysleno pro box-of-death...
end