local sensorInfo = {
	name = "",
	desc = "",
	author = "",
	date = "2017-05-16",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1 

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

-- @description 
return function(teamID, points)	
		
	targets = {}
	for i=1,#points do
		p = points[i]
		around = Spring.GetUnitsInSphere(p.x,p.y,p.z,300,teamID)
		for j=1,#around do
			def = Spring.GetUnitDefID(around[j])
			if def~=nil and UnitDefs[def].name~="corak" and UnitDefs[def].name~="corthud" and
				UnitDefs[def].name~="correap" and UnitDefs[def].name~="corpyro" and UnitDefs[def].name~="DCA" then
				targets[#targets+1] = around[j]
			end
		end
	end
	
	return targets
end