local sensorInfo = {
	name = "ExampleDebug",
	desc = "Sends data to example debug widget",
	author = "PepeAmpere",
	date = "2018-04-16",
	license = "MIT",
}

-- get madatory module operators
VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module

-- get other madatory dependencies
attach.Module(modules, "message") -- communication backend load

local EVAL_PERIOD_DEFAULT = math.huge -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

-- @description return current wind statistics
return function()
	if #units > 0 then
		local unitID = units[1]
		local x,y,z = Spring.GetUnitPosition(unitID)
				
		local hillX,hillY,hillZ
		local found = false
		local minH, maxH = Spring.GetGroundExtremes()
		for i=1, Game.mapSizeX do
			for j=1, Game.mapSizeZ do
				if(Spring.GetGroundHeight(i,j)==maxH) then
					hillX,hillY,hillZ = i,maxH,j
					found = true
					break
				end
			end
			if found then break end
		end	
		
		
		if (Script.LuaUI('exampleDebug_update')) then
			Script.LuaUI.exampleDebug_update(
				unitID, -- key
				{	-- data
					startPos = Vec3(x,y,z), 
					endPos = Vec3(hillX,hillY,hillZ)
				}
			)
		end
		return {	-- data
					startPos = Vec3(x,y,z), 
					endPos = Vec3(hillX,hillY,hillZ)
				}
	end
end