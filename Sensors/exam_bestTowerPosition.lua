local sensorInfo = {
	name = "",
	desc = "",
	author = "",
	date = "2017-05-16",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1 

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

-- @description 
return function(point, pathTargets, pathPoints)	
		
	myRange = 960--TODO:...
	
	bestPointScore = -1
	bestPoint = nil
	
	wantedHeight = Spring.GetGroundHeight(point.x,point.z)
	
	n = 1
	
	--find two nearest path points
	aPoint = nil
	bPoint = nil
	aDist = math.huge
	bDist = math.huge
	if pathPoints~=nil then
		for i=1,#pathPoints do
			if pathPoints[i]:Distance(point)<aDist then
				bPoint = aPoint
				bDist = aDist
				aDist = pathPoints[i]:Distance(point)
				aPoint = pathPoints[i]
			elseif pathPoints[i]:Distance(point)<bDist then
				bDist = pathPoints[i]:Distance(point)
				bPoint = pathPoints[i]
			end
		end
	end
	
	--try some points around the given point
	for i=-200,200,50 do
		for j=-200,200,50 do
				
			newPoint = Vec3(point.x+i,0,point.z+j)
			
			if pathPoints~=nil then
				bP = Vec3(bPoint.x,0,bPoint.z)
				aP = Vec3(aPoint.x,0,aPoint.z)
				q = bP - aP
				distFromPath = (q.z * newPoint.x - q.x * newPoint.z + bP.x*aP.z - bP.z*aP.x)/q:Length()
			else
				distFromPath = math.huge
			end
			
			h=Spring.GetGroundHeight(newPoint.x,newPoint.z)
			if h>=wantedHeight-50 and h<=wantedHeight+50 and distFromPath>700 then --consider them only if at the same height

				
				score = 0
				for k=1,#pathTargets do --cycle through all important targets
					if pathTargets[k]~=nil and Spring.ValidUnitID(pathTargets[k]) then --consider them only if they are alive
						def = Spring.GetUnitDefID(pathTargets[k])
						if def~=nil then
							enemyRange = UnitDefs[def].maxWeaponRange --TODO: really?
							a,b,c = Spring.GetUnitPosition(pathTargets[k])
							enemyPos = Vec3(a,0,c)
							if newPoint:Distance(enemyPos)<=enemyRange then --if the point is too close to some enemy do not build here ever
								score = -1
								break
							elseif newPoint:Distance(enemyPos)<=myRange then --if I can safely kill some enemy from this point, score++
								score = score + 1
							end
						end
					end
				end
				if score>=bestPointScore then --I want to find the point from which I can safely target the most enemies
					bestPointScore = score
					bestPoint = newPoint
				end
			end
		end
	end
	

	
	return bestPoint
	
end