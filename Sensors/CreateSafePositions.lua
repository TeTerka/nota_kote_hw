local sensorInfo = {
	name = "CreateSafePositions",
	desc = "",
	author = "tk",
	date = "2017-05-16",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = math.huge

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

return function(targets,squareSize)

--kazdemu manikovi dle jeho id priradi pozici na vylozeni
--je to proste ctverec, chci ho pro ttdr 7x7
	local safePositions = {}
	if #targets>squareSize*squareSize then return safePositions end 
	
	local info = Sensors.core.MissionInfo()
	local safeX = info.safeArea.center.x
	local safeY = info.safeArea.center.y
	local safeZ = info.safeArea.center.z
	local r = info.safeArea.radius * 0.7
	
	local step = 2*(r/math.sqrt(2))/(squareSize-1)
	local startX = safeX - (r/math.sqrt(2))
	local startZ = safeZ - (r/math.sqrt(2))
	
	local n =1
	for i=1,squareSize do
		for j=1,squareSize do
			if n<=#targets then
				safePositions[targets[n]] = Vec3(startX+((i-1)*step),safeY,startZ+((j-1)*step))
				n = n + 1
			end
		end
	end

	
	return safePositions
end