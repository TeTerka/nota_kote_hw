local sensorInfo = {
	name = "GetPos",
	desc = "",
	author = "",
	date = "2017-05-16",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1 

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

-- @description get position vector of some unit
return function(id)	
	
	x,y,z = Spring.GetUnitPosition(id)
	pos = Vec3(x,y,z)
	return pos
end