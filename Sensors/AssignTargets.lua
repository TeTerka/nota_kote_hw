local sensorInfo = {
	name = "AssignTargets",
	desc = "look for idle units an assign them some available targets",
	author = "tk",
	date = "2017-05-16",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = 0

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end


--actors are units that will be picking up targets

return function(actors,targets)

		--create assigned table
		if bb.assigned==nil then
			bb.assigned = {}
		end
			
		--try to find a target for every non-assigned actor
		for i=1,#actors do
			if bb.assigned[actors[i]]==nil then
				for j=1,#targets do
					if bb.assigned[targets[j]]==nil then
							bb.assigned[targets[j]] = actors[i]
							bb.assigned[actors[i]] = targets[j]
							break
					end
				end
			end
		end
		
	return 0
end