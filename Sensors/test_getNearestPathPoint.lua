local sensorInfo = {
	name = "PathPointDetector",
	desc = "",
	author = "",
	date = "2017-05-16",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1 

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

-- @description returns nearest path point of given path to given unit
return function(unitID, path)	
		
	--paths,_,_ = core.MissionInfo()
	--path = paths["middle"]
	x,y,z = Spring.GetUnitPosition(unitID)
	position = Vec3(x,y,z)
	bestPoint = nil
	bestDist = math.huge
	for i=1, #path do
		dist = position:Distance(path[i])
		if dist<bestDist then
			bestDist = dist
			bestPoint = path[i]
		end
	end
		
	return bestPoint
end