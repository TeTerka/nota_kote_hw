local sensorInfo = {
	name = "UnitFilter",
	desc = "",
	author = "tk",
	date = "2018-04-25",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

-- @description return filtered list of units
return function(allUnits,filterByName)	

		if allUnits==nil then return nil end

		local count = #allUnits
		local filteredUnits = {}
		local n = 1
		for i=1, count do
			local def = Spring.GetUnitDefID(allUnits[i])
			isOK = true
			for j=1,#filterByName do
				if def~=nil and UnitDefs[def].name == filterByName[j] then
					isOK = false
					break
				end
			end
			if isOK==true then
				filteredUnits[n] = allUnits[i]
				n = n+1
			end
		end
		return filteredUnits
		
end