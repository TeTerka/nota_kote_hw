local sensorInfo = {
	name = "AllyDetector",
	desc = "",
	author = "",
	date = "2017-05-16",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1 

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

-- @description is there an ally of given team nearby?
return function(idleUnits,size)	

	groupUnits = {}
	for i=1,size do
		groupUnits[#groupUnits+1] = idleUnits[i]
	end
	return groupUnits
	
end