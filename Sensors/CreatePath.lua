local sensorInfo = {
	name = "CreatePath",
	desc = "precomputes all the paths",
	author = "tk",
	date = "2017-05-16",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = math.huge -- jen jednou

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

--start is a position
--targets is a list of target positions
--grid is the result of EvaluateGrid sensor
return function(start, targets, grid)
		
	--get closet grid position to start position
	local sX = math.floor(start.x/100)--now only for fixed grid size == 100
	local sZ = math.floor(start.z/100)
	
	local paths = {}
	
	for i=1,#targets do
		local grid2 = grid--reset the whole grid
		for a=1, Game.mapSizeX, 100 do
			local aa = math.floor(a/100)
			for b=1, Game.mapSizeZ, 100 do
				local bb = math.floor(b/100)
				if grid2[aa]~=null and grid2[aa][bb]~=11 and grid2[aa][bb]~=111 then grid2[aa][bb] = 1 end
			end
		end
		
		--get closet grid position to target position
		local pointX, pointY, pointZ = Spring.GetUnitPosition(targets[i])
		local gX = math.floor(pointX/100)
		local gZ = math.floor(pointZ/100)
	
		local queueFirst = 1
		local queueLast = 1
		local queue = {[1] = {sX,sZ,nil,nil}}
	
		local found = false
		local foundX, foundZ = 0,0
		--uzel ve tvaru {x,z,parentX,parentZ} kde x,z nejsou souradnice na mape, ale spis indexy do gridu
		while queueFirst<=queueLast do
			--pop
			local current = queue[queueFirst]
			queueFirst = queueFirst + 1
			--check goal condition
			if current[1]>= gX-2 and current[1]<=gX+2 and current[2]>=gZ-2 and current[2]<=gZ+2 then
				found = true
				foundX = current[1]
				foundZ = current[2]
				break;
			end
			--add neighbours to queue
			if grid2[current[1]+1]~=nil and grid2[current[1]+1][current[2]]==1 then  --(1 = no enemies, 11 = dangerous, {x,y} = visited)
				queueLast = queueLast + 1
				queue[queueLast] = {current[1]+1,current[2],current[1],current[2]}
				grid2[current[1]+1][current[2]] = {current[1],current[2]}--write down the parent for the grid cell
			end
			if grid2[current[1]-1]~=nil and grid2[current[1]-1][current[2]]==1 then 
				queueLast = queueLast + 1
				queue[queueLast] = {current[1]-1,current[2],current[1],current[2]}
				grid2[current[1]-1][current[2]] = {current[1],current[2]}
			end
			if grid2[current[1]]~=nil and grid2[current[1]][current[2]+1]==1 then 
				queueLast = queueLast + 1
				queue[queueLast] = {current[1],current[2]+1,current[1],current[2]}
				grid2[current[1]][current[2]+1] = {current[1],current[2]}
			end
			if grid2[current[1]]~=nil and grid2[current[1]][current[2]-1]==1 then 
				queueLast = queueLast + 1
				queue[queueLast] = {current[1],current[2]-1,current[1],current[2]}
				grid2[current[1]][current[2]-1] = {current[1],current[2]}
			end
		end
	
		
		--create the path
		local path = {}
		if found==true then
			local x = foundX
			local z = foundZ
			local n = 1
			while x~=sX or z~=sZ do
				path[n] = Vec3(x*100,0,z*100)
				x = grid2[x][z][1]
				z = grid2[x][z][2]
				n = n+1
			end
		end
		
		paths[targets[i]] = path --assigning paths to targets by the targets ID...
	end
	
	return paths

end