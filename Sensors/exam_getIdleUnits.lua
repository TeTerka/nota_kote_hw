local sensorInfo = {
	name = "AllyDetector",
	desc = "",
	author = "",
	date = "2017-05-16",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1 

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

return function(allUnits)	

	--if bb.groups==nil or #bb.groups==0 then return {} end
	
	idleUnits = {}
	for i=1,#allUnits do
		found = false
		for groupName,groupUnits in pairs(bb.groups) do
			for j=1,#groupUnits do
				if allUnits[i]==groupUnits[j] then
					found=true
					break
				end
			end
			if found then break end
		end
		if found==false then
			idleUnits[#idleUnits+1] = allUnits[i]
		end
	end
	
	return idleUnits
end