local sensorInfo = {
	name = "GetPlatforms",
	desc = "",
	author = "tk",
	date = "2018-04-25",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = math.huge -- jenom jednou

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

-- speedups
local SpringGetWind = Spring.GetWind

-- @description return the platform positions for ctp2
return function()
	local spots = {}
	local minH, maxH = Spring.GetGroundExtremes()
	local platformWidthX = 160 
	local platformWidthZ = 80
	local n = 0
	for i=1, Game.mapSizeX do
		for j=1, Game.mapSizeZ do
			if(Spring.GetGroundHeight(i,j)==maxH) then
				local shouldIgnore = false;
				for k = 1, n do
					if (math.abs(spots[k].x - i)<=platformWidthX/2)
						and (math.abs(spots[k].z - j)<=platformWidthZ/2) then
							shouldIgnore = true
							break
					end
				end
				if shouldIgnore then break end
				
				n = n +1
				spots[n] = Vec3(i+(platformWidthX/2),Spring.GetGroundHeight(i,j),j+(platformWidthZ/2))
				break
			end
		end
	end
	return spots
end