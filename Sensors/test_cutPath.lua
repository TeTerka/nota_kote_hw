local sensorInfo = {
	name = "PathCutter",
	desc = "",
	author = "",
	date = "2017-05-16",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1 

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

-- @description returns patr of the path
return function(from, to, path)	
		
	--paths,_,_ = core.MissionInfo()
	--path = paths["middle"]
	if to==nil then
		to = path[#path]--TODO: prozatim...
	end

	newPath = {}
	copy = false
	backwards = false
	for i=1, #path do
		if path[i]==from then
			copy = true
		end
		if copy then
			newPath[#newPath+1] = path[i]
		end
		if path[i]==to then
			if copy==false then backwards=true end --neboli pokud jsme na to narazili driv nez na from
			break
		end
	end

	if backwards==true then
		for i=#path,1,-1 do
			if path[i]==from then
				copy = true
			end
			if copy then
				newPath[#newPath+1] = path[i]
			end
			if path[i]==to then
				break
			end
		end
	end
		
	return newPath
end